package com.hxh.rabbitmq.accdient.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/11 9:52 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class OrderDto {

    /**
     * 订单编号
     */
    private String id;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 订单金额
     */
    private BigDecimal amount;

    /**
     * 交易时间
     */
    private String tradeTime;

}
