package com.hxh.rabbitmq.accdient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqAccdientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqAccdientApplication.class, args);
    }

}
